from builtins import object

from django.http import HttpResponseRedirect
from django.template.context_processors import csrf
from django.urls import reverse
from django.views.generic import ListView
from rec.models import Recept, RecipeIngredients, dCookingSteps
from rec.forms import AddReceptForm, AddRecipeIngredientsForm, AddCookingStepsForm
from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from django.db.models import Q


def SearchKeyword(request):
    """Функция поиска по основным моделям"""

    template_name = 'rec/search_list.html'

    c = {}
    c.update(csrf(request))

    c['keyword'] = request.POST.get('keyword', False)

    if c['keyword'] is not False:
        c['search_result_recept'] = Recept.objects.filter(
            Q(title__icontains=c['keyword'])
            | Q(skillStatus__title__icontains=c['keyword'])
            | Q(typeOfDishes__title__icontains=c['keyword'])
            | Q(timeForPreparing__contains=c['keyword']))

        c['search_result_ingri'] = RecipeIngredients.objects.filter(Q(ingredient__title__icontains=c['keyword']))
        c['search_result_steps'] = dCookingSteps.objects.filter(Q(title__icontains=c['keyword']))

    return render(request, template_name, c)


class ReceptObjectMixin(object):
    model = Recept

    def get_object(self):
        id = self.kwargs.get('id')
        obj = None
        if id is not None:
            obj = get_object_or_404(self.model, id=id)
        return obj


class ReceptDeleteView(ReceptObjectMixin, View):
    template_name = "rec/recept_delete.html"  # DetailView

    def get(self, request, id=None, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            context['object'] = obj
        return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        # POST method
        context = {}
        obj = self.get_object()
        if obj is not None:
            obj.delete()
            context['object'] = None
            return redirect('/')
        return render(request, self.template_name, context)


class ReceptUpdateView(ReceptObjectMixin, View):
    template_name = "rec/recept_create.html"  # DetailView

    def get_object(self):
        id = self.kwargs.get('id')
        obj = None
        if id is not None:
            obj = get_object_or_404(Recept, id=id)
        return obj

    def get(self, request, id=None, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = AddReceptForm(instance=obj)
            context['recept'] = obj
            context['ingridients'] = RecipeIngredients.objects.filter(recept=obj)
            context['steps'] = dCookingSteps.objects.filter(recept=obj)
            context['form'] = form
        return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        # POST method
        context = {}
        obj = self.get_object()

        if obj is not None:
            form = AddReceptForm(request.POST, request.FILES, instance=obj)

            if form.is_valid():
                recept = form.save(commit=False)
                recept.save()

            context['recept'] = obj
            context['form'] = form

        btnClick = request.POST['btnClick'];

        if btnClick == 'Ingri':
            return HttpResponseRedirect(reverse('ingri-create', kwargs={"id": id}))
        elif btnClick == 'Step':
            return HttpResponseRedirect(reverse('step-create', kwargs={"id": id}))
        else:
            return render(request, self.template_name, context)


class ReceptCreateView(View):
    template_name = "rec/recept_create.html"  # DetailView

    def get(self, request, *args, **kwargs):
        # GET method
        form = AddReceptForm()
        context = {"form": form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        # POST method
        form = AddReceptForm(request.POST, request.FILES)
        if form.is_valid():
            fm = form.save()
            form = AddReceptForm()
        context = {"form": form}
        id_ = fm.id

        btnClick = request.POST['btnClick'];

        if btnClick == 'Ingri':
            return HttpResponseRedirect(reverse('ingri-create', kwargs={"id": id_}))
        elif btnClick == 'Step':
            return HttpResponseRedirect(reverse('step-create', kwargs={"id": id_}))
        else:
            return render(request, self.template_name, context)


class ReceptListView(ListView):
    template_name = 'rec/recept_list.html'
    paginate_by = 5
    queryset = Recept.objects.all()


class ReceptDetailView(ReceptObjectMixin, View):
    template_name = "rec/recept_detail.html"  # DetailView

    def get(self, request, id=None, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        context['recept'] = obj
        context['ingridients'] = RecipeIngredients.objects.filter(recept=obj)
        context['cookingsteps'] = dCookingSteps.objects.filter(recept=obj)
        return render(request, self.template_name, context)


class IndexView(View):
    template_name = "rec/index.html"

    def get(self, request, id=None, *args, **kwargs):
        # GET method
        context = {}
        return render(request, self.template_name, context)


class IngridCreateView(ReceptObjectMixin, View):
    template_name = "rec/ingredient_create.html"

    def get(self, request, *args, **kwargs):
        # GET method
        obj_recept = self.get_object()
        form = AddRecipeIngredientsForm(initial={'recept': obj_recept})
        context = {"form": form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        # POST method
        form = AddRecipeIngredientsForm(request.POST)

        if form.is_valid():
            fm = form.save()
            id_ = fm.recept_id  # fm.cleaned_data['recept']

            return HttpResponseRedirect(reverse('recept-update', kwargs={"id": id_}))
        else:
            form = AddRecipeIngredientsForm()
            context = {"form": form}

            return render(request, self.template_name, context)


class IngridUpdateView(View):
    template_name = "rec/ingredient_create.html"  # DetailView

    def get_object(self):
        id = self.kwargs.get('id')
        obj = None
        if id is not None:
            obj = get_object_or_404(RecipeIngredients, id=id)
        return obj

    def get(self, request, id=None, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = AddRecipeIngredientsForm(instance=obj)
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        # POST method
        obj = self.get_object()

        if obj is not None:
            form = AddRecipeIngredientsForm(request.POST, instance=obj)

            if form.is_valid():
                fm = form.save()
                id_ = fm.recept_id
                return HttpResponseRedirect(reverse('recept-update', kwargs={"id": id_}))
            else:
                form = AddRecipeIngredientsForm()
                context = {"form": form}
                return render(request, self.template_name, context)


class IngridDeleteView(View):
    template_name = "rec/ingredient_delete.html"

    def get_object(self):
        id = self.kwargs.get('id')
        obj = None
        if id is not None:
            obj = get_object_or_404(RecipeIngredients, id=id)
        return obj

    def get(self, request, id=None, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            context['object'] = obj
        return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        # POST method
        context = {}
        obj = self.get_object()
        if obj is not None:
            obj.delete()
            context['object'] = None
            return redirect('/')
        return render(request, self.template_name, context)


class StepCreateView(ReceptObjectMixin, View):
    template_name = "rec/ingredient_create.html"

    def get(self, request, *args, **kwargs):
        # GET method
        obj_recept = self.get_object()
        form = AddCookingStepsForm(initial={'recept': obj_recept})
        context = {"form": form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        # POST method
        form = AddCookingStepsForm(request.POST)

        if form.is_valid():
            fm = form.save()
            id_ = fm.recept_id  # fm.cleaned_data['recept']

            return HttpResponseRedirect(reverse('recept-update', kwargs={"id": id_}))
        else:
            form = AddCookingStepsForm()
            context = {"form": form}

            return render(request, self.template_name, context)


class StepUpdateView(View):
    template_name = "rec/ingredient_create.html"  # DetailView

    def get_object(self):
        id = self.kwargs.get('id')
        obj = None
        if id is not None:
            obj = get_object_or_404(dCookingSteps, id=id)
        return obj

    def get(self, request, id=None, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = AddCookingStepsForm(instance=obj)
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        # POST method
        obj = self.get_object()

        if obj is not None:
            form = AddCookingStepsForm(request.POST, instance=obj)

            if form.is_valid():
                fm = form.save()
                id_ = fm.recept_id
                return HttpResponseRedirect(reverse('recept-update', kwargs={"id": id_}))
            else:
                form = AddRecipeIngredientsForm()
                context = {"form": form}
                return render(request, self.template_name, context)


class StepDeleteView(View):
    template_name = "rec/ingredient_delete.html"

    def get_object(self):
        id = self.kwargs.get('id')
        obj = None
        if id is not None:
            obj = get_object_or_404(dCookingSteps, id=id)
        return obj

    def get(self, request, id=None, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            context['object'] = obj
        return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        # POST method
        context = {}
        obj = self.get_object()
        if obj is not None:
            obj.delete()
            context['object'] = None
            return redirect('/')
        return render(request, self.template_name, context)

# def post(request, *args, **kwargs):
#     return render(request, 'about.html', {})

# class ReceptListView(ListView):
#     template_name = 'rec/recept_list.html'
#     paginate_by = 5
#     queryset = Recept.objects.all()
#
#
# class ReceptDetailView(DetailView):
#     template_name = 'rec/recept_detail.html'
#
#     def get_object(self, queryset=None):
#         id_ = self.kwargs.get("id")
#         return get_object_or_404(Recept, id=id_)
#
#
# class ReceptCreateView(CreateView):
#     template_name = 'rec/recept_create.html'
#     form_class = AddReceptForm
#     queryset = Recept.objects.all()
#
#     def form_valid(self, form):
#         print(form.cleaned_data)
#         return super().form_valid(form)
#
#     def post(self, request, *args, **kwargs):
#         self.form = AddReceptForm(request.POST)
#
#         if self.form.is_valid():
#             fm = self.form.save()
#             id_ = fm.id
#             btnClick = request.POST['btnClick'];
#
#             if btnClick == 'Ingri':
#                 return HttpResponseRedirect(reverse('recept-detail', kwargs={"id": id_}))
#             elif btnClick == 'Save':
#                 return HttpResponseRedirect(reverse('recept-list'))
#
#             # return redirect('/'+str(id_))
#         else:
#             return super(ReceptCreateView, self).get(request, *args, **kwargs)
#
#
# class ReceptUpdateView(UpdateView):
#     template_name = 'rec/recept_create.html'
#     form_class = AddReceptForm
#
#     def get_object(self, queryset=None):
#         id_ = self.kwargs.get("id")
#         return get_object_or_404(Recept, id=id_)
#
#     def form_valid(self, form):
#         print(form.cleaned_data)
#         return super().form_valid(form)
#
#
# class ReceptDeleteView(DeleteView):
#     template_name = 'rec/recept_delete.html'
#
#     def get_object(self, queryset=None):
#         id_ = self.kwargs.get("id")
#         return get_object_or_404(Recept, id=id_)
#
#     def get_success_url(self):
#         return reverse('recept-list')

# class Index(TemplateView):
#     template_name = 'rec/index.html'

# class ReceptListView(TemplateView):
#     template_name = 'rec/index.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(ReceptListView, self).get_context_data(**kwargs)
#         context['recepts'] = Recept.objects.all()
#
#         return context


# class AddReceptView(TemplateView):
#     """Добавление Рецепта"""
#
#     model = Recept
#     form_class = AddReceptForm
#     template_name = 'rec/recreate.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(AddReceptView, self).get_context_data(**kwargs)
#
#         if self.request.method == 'POST':
#             recreate_form = AddReceptForm(data=self.request.POST)
#
#             if recreate_form.is_valid():
#                 recreate_form.save()
#                 return redirect("/")
#             else:
#                 print(recreate_form.errors)
#         else:
#             context['recreate_form'] = AddReceptForm()
#
#         return context


# class AddReceptView(CreateView):
#     """Добавление Рецепта"""
#
#     model = Recept
#     form_class = AddReceptForm
#     template_name = 'rec/recreate.html'
#
#     def form_valid(self, form):
#         form.save()
#         return redirect("/")
#
#     def success_url(self):
#         return redirect("/add-recept/")

# Рабочий кусок
# class AddRecipeIngredientsView(CreateView):
#     """Добавление ингредиента рецепта"""
#
#     model = RecipeIngredients
#     form_class = AddRecipeIngredientsForm
#     template_name = 'rec/ingredient_create.html'
#
#     def form_valid(self, form):
#         form.save()
#         return redirect("/")
#
#     def success_url(self):
#         return redirect("/add-recept/")


# class ReceptDetailView(TemplateView):
#     template_name = 'rec/recept.html'

# class ReceptCreateView(TemplateView):
#     """ Это комментарии для документации"""
#
#     template_name = 'rec/recreate.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(ReceptCreateView, self).get_context_data(**kwargs)
#
#         if self.request.method == 'POST':
#             recreate_form = AddReceptForm(data=self.request.POST)
#
#             if recreate_form.is_valid():
#                 recreate_form.save()
#                 return redirect('rec/index.html')
#             else:
#                 print(recreate_form.errors)
#         else:
#             context['recreate_form'] = AddReceptForm()
#
#         return context
