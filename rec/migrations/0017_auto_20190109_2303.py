# Generated by Django 2.1.3 on 2019-01-09 20:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rec', '0016_auto_20181231_0203'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dcookingsteps',
            options={'verbose_name': 'Шаг приготовления', 'verbose_name_plural': 'Шаги приготовления'},
        ),
        migrations.RenameField(
            model_name='dcookingsteps',
            old_name='TextStep',
            new_name='title',
        ),
        migrations.AlterField(
            model_name='recipeingredients',
            name='ingredient',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='rec.Ingridient', verbose_name='Ингредиент'),
        ),
    ]
