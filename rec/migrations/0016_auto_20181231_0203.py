# Generated by Django 2.1.3 on 2018-12-30 23:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rec', '0015_auto_20181231_0150'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipeingredients',
            name='ingredient',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='rec.Ingridient', verbose_name='Ингредиент'),
        ),
    ]
