from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from rec import views as recviews
from users import views as usersviews

urlpatterns = [
    path('', recviews.IndexView.as_view(), name='index'),
    path('recepts/', recviews.ReceptListView.as_view(), name='recept-list'),
    path('create/', recviews.ReceptCreateView.as_view(), name='recept-create'),
    path('search/', recviews.SearchKeyword, name='search'),
    path('<int:id>/update/', recviews.ReceptUpdateView.as_view(), name='recept-update'),
    path('<int:id>/delete/', recviews.ReceptDeleteView.as_view(), name='recept-delete'),
    path('<int:id>', recviews.ReceptDetailView.as_view(), name='recept-detail'),
    path('<int:id>/create-ingri/', recviews.IngridCreateView.as_view(), name='ingri-create'),
    path('<int:id>/update-ingri/', recviews.IngridUpdateView.as_view(), name='ingri-update'),
    path('<int:id>/delete-ingri/', recviews.IngridDeleteView.as_view(), name='ingri-delete'),
    path('<int:id>/create-step/', recviews.StepCreateView.as_view(), name='step-create'),
    path('<int:id>/update-step/', recviews.StepUpdateView.as_view(), name='step-update'),
    path('<int:id>/delete-step/', recviews.StepDeleteView.as_view(), name='step-delete'),
    path('login/', usersviews.Login.as_view(), name='login'),
    path('register/', usersviews.Register.as_view(), name='register'),
    path('logout/', usersviews.Logout.as_view(), name='logout'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
