from django.db import models
from django.urls import reverse


class Ingridient(models.Model):
    """Справочник ингредиентов"""

    title = models.CharField(
        max_length=80,
        null=False,
        blank=False)

    class Meta:
        db_table = 'ingridient'
        verbose_name = 'Ингредиент'
        verbose_name_plural = "Ингридиенты"

    def __str__(self):
        return self.title


class dSkillStatus(models.Model):
    """Справочник уровней мастерства"""

    title = models.CharField(
        max_length=80,
        unique=True,
        null=False,
        blank=False)

    class Meta:
        db_table = 'skillstatus'
        verbose_name = 'Уровень мастерства'
        verbose_name_plural = 'Уровни мастерства'

    def __str__(self):
        return self.title


class dTypeOfDishes(models.Model):
    """Справочник типы блюд"""

    title = models.CharField(
        max_length=80,
        unique=True,
        null=False,
        blank=False)

    class Meta:
        db_table = 'typeofdishes'
        verbose_name = 'Тип блюд'
        verbose_name_plural = 'Типы блюд'

    def __str__(self):
        return self.title


class Recept(models.Model):
    """Класс рецептов"""

    dataCreateRecept = models.DateTimeField(auto_now_add=True)
    dataLastEditRecept = models.DateTimeField(auto_now=True)

    title = models.CharField(
        max_length=80,
        default='Кулинарный рецепт',
        null=True,
        blank=True)
    image = models.FileField(
        null=True,
        blank=True)
    skillStatus = models.ForeignKey(
        dSkillStatus,
        null=True,
        verbose_name="Уровень мастерства",
        on_delete=models.CASCADE)
    typeOfDishes = models.ForeignKey(
        dTypeOfDishes,
        null=True,
        verbose_name="Тип блюда",
        on_delete=models.CASCADE)
    timeForPreparing = models.PositiveIntegerField(null=True)

    def get_absolute_url(self):
        return reverse("recept-detail", kwargs={"id": self.id})

    class Meta:
        db_table = 'recepts'
        verbose_name = 'Рецепт'
        verbose_name_plural = "Рецепты"

    def __str__(self):
        return self.title


class RecipeIngredients(models.Model):
    """Таблица с ингредиентами рецепта и количеством"""

    recept = models.ForeignKey(
        Recept,
        null=True,
        verbose_name="Рецепт",
        on_delete=models.CASCADE)
    ingredient = models.ForeignKey(
        Ingridient,
        null=True,
        verbose_name="Ингредиент",
        on_delete=models.CASCADE)
    kol = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name="Количество",
        null=True)

    class Meta:
        db_table = 'recipeingredients'
        verbose_name = 'Игредиент рецептуры'
        verbose_name_plural = "Ингредиенты рецептуры"


class dImages(models.Model):
    pathImage = models.CharField(max_length=255, default='/images/')
    nameFile = models.CharField(max_length=80, default='im_')
    recept = models.ForeignKey(Recept, null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        db_table = 'pathimages'
        verbose_name = 'Path of images'

    def __str__(self):
        return self.pathImage


class dCookingSteps(models.Model):
    title = models.CharField(max_length=80, unique=True, null=False, blank=False)
    recept = models.ForeignKey(Recept, null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        db_table = 'cookingstep'
        verbose_name = 'Шаг приготовления'
        verbose_name_plural = "Шаги приготовления"

    def __str__(self):
        return self.TextStep


class dEdIzmer(models.Model):
    name = models.CharField(max_length=80, unique=True, null=False, blank=False)

    class Meta:
        db_table = 'edizmer'
        verbose_name = 'Единица_изменения'
        verbose_name_plural = 'Единицы_изменения'

    def __str__(self):
        return self.name

# class ListIngrid(models.Model):
#     recept = models.ForeignKey(Recept, null=True, blank=False, on_delete=models.SET_NULL)
#     ingrid = models.ForeignKey(dIngridients, null=True, blank=False, on_delete=models.SET_NULL)
#     edizmer = models.ForeignKey(dEdIzmer, null=True, blank=True, on_delete=models.SET_NULL)
#     kolich = models.DecimalField(max_digits=10, decimal_places=0, null=True)
#     createdby = models.DateTimeField(auto_now_add=True, null=True)
#     chengedby = models.DateTimeField(auto_now=True)
#
#     class Meta:
#         db_table = 'listingrid'
#         verbose_name = 'ListIngrid'
#
#     def __str__(self):
#         return '%s, %s, %s' % (self.pk, self.recept, self.ingrid)
