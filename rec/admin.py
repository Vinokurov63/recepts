from django.contrib import admin
from rec.models import Recept, dSkillStatus, dTypeOfDishes, Ingridient, RecipeIngredients, dCookingSteps


class ReceptsAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'image', 'skillStatus', 'typeOfDishes', 'timeForPreparing', 'dataCreateRecept',
                    'dataLastEditRecept']
    list_filter = ['dataCreateRecept', 'dataLastEditRecept']
    list_editable = ['image', 'timeForPreparing']
    # prepopulated_fields = {'slaug': ('title',)} составное поле обычно slaug


class RecipeIngredientsAdmin(admin.ModelAdmin):
    list_display = ['recept', 'ingredient', 'kol']
    list_filter = ['recept']
    list_editable = ['kol']


class CookingStepsAdmin(admin.ModelAdmin):
    list_display = ['id', 'recept', 'title']
    list_filter = ['recept']
    list_editable = ['recept', 'title']


admin.site.register(Recept, ReceptsAdmin)
admin.site.register(RecipeIngredients, RecipeIngredientsAdmin)
admin.site.register(dCookingSteps, CookingStepsAdmin)
admin.site.register(Ingridient)
admin.site.register(dSkillStatus)
admin.site.register(dTypeOfDishes)
