from django import forms
from rec.models import Recept, dSkillStatus, dTypeOfDishes, RecipeIngredients, Ingridient, dCookingSteps


class AddReceptForm(forms.ModelForm):
    """Форма добавления рецепта"""

    class Meta:
        model = Recept
        fields = '__all__'

    title = forms.CharField(
        initial='Рецепт №',
        label="Название рецепта")
    image = forms.FileField(
        label="Фото блюда",
        allow_empty_file=None)
    skillStatus = forms.ModelChoiceField(
        queryset=dSkillStatus.objects.all(),
        label="Уровень мастерства",
        empty_label=None)
    typeOfDishes = forms.ModelChoiceField(
        queryset=dTypeOfDishes.objects.all(),
        label="Тип блюда",
        empty_label=None)
    timeForPreparing = forms.IntegerField(
        initial=60,
        label="Время приготовления",
        help_text="минут")


class AddRecipeIngredientsForm(forms.ModelForm):
    """Форма добавления ингредиентов в рецепт"""

    class Meta:
        model = RecipeIngredients
        fields = '__all__'

    recept = forms.ModelChoiceField(
        widget=forms.HiddenInput,
        queryset=Recept.objects.all(),
        label='Рецепт',
        empty_label=None)

    ingredient = forms.ModelChoiceField(
        queryset=Ingridient.objects.all(),
        label="Ингредиент рецепта",
        empty_label=None)
    kol = forms.FloatField(
        initial=100,
        label="Количество",
        help_text="гр.")


class AddCookingStepsForm(forms.ModelForm):
    """Форма добавления шагов приготовления в рецепт"""

    class Meta:
        model = dCookingSteps
        fields = '__all__'

    title = forms.CharField(
        label="Шаг приготовления")

    recept = forms.ModelChoiceField(
        widget=forms.HiddenInput,
        queryset=Recept.objects.all(),
        label='Рецепт',
        empty_label=None)
