# -*- coding: utf-8 -*-
from django import forms
from django.forms import (
    TextInput, CharField, Select, RadioSelect, Textarea, CheckboxInput
)
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import gettext_lazy as _


# from django import forms
#
#
# class UserForm(forms.Form):
#     name = forms.CharField(widget=forms.TextInput(attrs={"class": "myfield"}))
#     age = forms.IntegerField(widget=forms.NumberInput(attrs={"class": "myfield"}))
#

class LoginForm(AuthenticationForm):
    """
    Form that manages the User model fields.
    """
    username = forms.CharField(label='Имя пользователя', widget=TextInput(attrs={
        'class': 'form-control input-md',
        'style': 'width: 100%; display: inline;'
    }), required=True)

    password = forms.CharField(label=' Пароль', widget=TextInput(attrs={
        'class': 'form-control input-md',
        'style': 'width: 100%; display: inline;',
        'type': 'password',
    }), required=True)

    class Meta:
        model = User

        fields = [
            'username',
            'password',
        ]


class RegisterForm(UserCreationForm):
    """
    Form that manages the User creation.
    """
    username = forms.CharField(label='Логин', widget=TextInput(attrs={
        # 'value': 'aosorio',
        'class': 'form-control input-md',
        'style': 'width: 100%; display: inline;',
    }), required=True)

    first_name = forms.CharField(label='Имя', widget=TextInput(attrs={
        # 'value': 'argenis',
        'class': 'form-control input-md',
        'style': 'width: 100%; display: inline;',
    }), required=True)

    last_name = forms.CharField(label='Фамилия', widget=TextInput(attrs={
        # 'value': 'osorio',
        'class': 'form-control input-md',
        'style': 'width: 100%; display: inline;',
    }), required=True)

    email = forms.CharField(label='E-mail', widget=TextInput(attrs={
        # 'value': 'aosorio@mail.com',
        'class': 'form-control input-md',
        'style': 'width: 100%; display: inline;',
    }), required=True)

    password1 = forms.CharField(label='Пароль', widget=TextInput(attrs={
        # 'value': 'arka.1234-d',
        'class': 'form-control input-md',
        'style': 'width: 100%; display: inline;', 'type': 'password',
    }), required=True)

    password2 = forms.CharField(label='пароль еще раз...', widget=TextInput(attrs={
        # 'value': 'arka.1234-d',
        'class': 'form-control input-md',
        'style': 'width: 100%; display: inline;', 'type': 'password',
    }), required=True)

    def save(self, commit=True):
        """
        Method that saves the data from the form.
        Active users is default.
        """
        user = User.objects.create_user(self.cleaned_data['username'],
                                        self.cleaned_data['email'],
                                        self.cleaned_data['password1'])
        # user.is_active = False
        # user.is_active = True
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()
        return user

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')
